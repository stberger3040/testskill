# Skill calls will begin at the evaluate() method, with the skill payload passed
# as 'payload' and the skill context passed as 'context'.
# For more details, see the AIOS User Manual.

def evaluate(payload: dict, context: dict) -> dict:
    return {'response': payload["request"]}
